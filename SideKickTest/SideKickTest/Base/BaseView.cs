﻿using ReactiveUI;
using ReactiveUI.XamForms;
using System;
using System.Collections.Generic;
using System.Text;

namespace SideKickTest.Base
{
    public class BaseView<TViewModel> : ReactiveContentView<TViewModel> where TViewModel : BaseViewModel
    {
        public BaseView()
        {
            ViewModel = InstanceViewModel();
            BindingContext = ViewModel;
            this.WhenActivated(registerDisposable => CreateBindings(registerDisposable));
        }

        protected virtual void CreateBindings(Action<IDisposable> registerDisposable)
        { }

        private TViewModel InstanceViewModel() => Activator.CreateInstance<TViewModel>();                
    }
}
