﻿using ReactiveUI;
using ReactiveUI.XamForms;
using System;

namespace SideKickTest.Base
{
    public abstract class BasePage<TViewModel> : ReactiveContentPage<TViewModel> where TViewModel : BaseViewModel
    {
        public BasePage()
        {
            ViewModel = InstanceViewModel();
            BindingContext = ViewModel;            
            this.WhenActivated(registerDisposable => CreateBindings(registerDisposable));
        }

        protected virtual void CreateBindings(Action<IDisposable> registerDisposable)
        { }
                    
        private TViewModel InstanceViewModel() => Activator.CreateInstance<TViewModel>();
        

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ViewModel?.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            ViewModel?.OnDissapearing();
        }
    }
}
