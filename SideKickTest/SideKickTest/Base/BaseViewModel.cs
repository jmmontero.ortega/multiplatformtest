﻿using System;
using ReactiveUI;
using Xamarin.Forms;

namespace SideKickTest.Base
{
    public abstract class BaseViewModel : ReactiveObject
    {
        public INavigation Navigation => App.Current.MainPage.Navigation;
        public BaseViewModel()
        {
            InitializeCommands();
        }

        protected virtual void InitializeCommands()
        { }
                    
        public virtual void OnAppearing()
        { }

        public virtual void OnDissapearing()
        { }
    }
}
