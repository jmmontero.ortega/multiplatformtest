﻿using ReactiveUI;
using SideKickTest.Base;
using SideKickTest.Features.Main;
using System;
using System.Collections.Generic;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace SideKickTest.Features.Login
{
    public class LoginViewModel : BaseViewModel
    {
        private string greetingText;
        private bool mainMenuVisibility;

        private ReactiveCommand<Unit,Unit> commandNavigateToMenu;
        private ReactiveCommand<Unit, Unit> commandTapOutside;

        public string GreetingText
        {
            get => greetingText;
            set => this.RaiseAndSetIfChanged(ref greetingText, value);
        }

        public bool MainMenuVisibility
        {
            get => mainMenuVisibility;
            set => this.RaiseAndSetIfChanged(ref mainMenuVisibility, value);
        }


        public ICommand CommandNavigateToMenu => commandNavigateToMenu;
        public ICommand CommandTapOutside => commandTapOutside;
        
        public LoginViewModel()
        {
            MessagingCenter.Subscribe<MainViewModel>(this, "Back", (s) => PerformTapOutside());
        }

        protected override void InitializeCommands()
        {
            base.InitializeCommands();
            commandNavigateToMenu = ReactiveCommand.CreateFromTask(PerformNavigateToMenu);
            commandTapOutside = ReactiveCommand.CreateFromTask(PerformTapOutside);
        }

        private Task PerformNavigateToMenu()
        {
            MainMenuVisibility = true;
            return Task.FromResult(Unit.Default);
        }
        
        private Task PerformTapOutside()
        {
            MainMenuVisibility = false;
            return Task.FromResult(Unit.Default);
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            GreetingText = $"Hello World, it is now {DateTime.Now}";
        }
    }
}
