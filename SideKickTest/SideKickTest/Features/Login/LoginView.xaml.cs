﻿using ReactiveUI;
using ReactiveUI.XamForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SideKickTest
{
    public partial class LoginView
    {
        public LoginView()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await AnimationAppearing();
        }

        protected override void CreateBindings(Action<IDisposable> d)
        {
            d(this.OneWayBind(ViewModel, vm => vm.GreetingText, v => v.LabelGreetings.Text));
            d(this.OneWayBind(ViewModel, vm => vm.CommandNavigateToMenu, v => v.ButtonNavigateToStart.Command));
            d(this.OneWayBind(ViewModel, vm => vm.MainMenuVisibility, v => v.MainMenu.IsVisible));
            d(this.OneWayBind(ViewModel, vm => vm.MainMenuVisibility, v => v.GridBlurred.IsVisible));
            d(this.OneWayBind(ViewModel, vm => vm.CommandTapOutside, v => v.TapGridBlurred.Command));

            d(this.WhenAnyValue(vm => vm.ViewModel.MainMenuVisibility)
                .ObserveOn(RxApp.MainThreadScheduler)
                .Subscribe(v =>
                {
                    if(v)
                    {
                        Device.BeginInvokeOnMainThread(() => AnimateMenu());
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() => DisableMenu());
                    }
                }));            
        }        
        private async Task AnimationAppearing()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await Task.Delay(100);
                await AnimateElement(LabelGreetings);
                await AnimateElement(ImageAnimal);
                await AnimateElement(ButtonNavigateToStart);
            });            
        }

        private async Task AnimateElement(VisualElement visualElement)
        {
            var taskFade = visualElement.FadeTo(1, 400, Easing.CubicOut);
            var taskTranslate = visualElement.TranslateTo(0, -50, 400, Easing.CubicOut);

            await Task.WhenAll(taskFade, taskTranslate);
        }

        private async void AnimateMenu()
        {            
            await Task.WhenAll(
                    GridBlurred.FadeTo(0.5, 400),
                    MainMenu.TranslateTo(0, 0, 400)
                ).ContinueWith(async t => await MainMenu.AnimationAppearing());            
        }

        private void DisableMenu()
        {
            MainMenu.TranslationX = -200;
            MainMenu.BackToAnimation();
        }        
    }
}
