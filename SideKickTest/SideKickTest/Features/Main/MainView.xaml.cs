﻿using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SideKickTest.Main
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainView 
	{
		public MainView ()
		{
			InitializeComponent ();              
		}

        protected override void CreateBindings(Action<IDisposable> d)
        {
            base.CreateBindings(d);

            d(this.OneWayBind(ViewModel, vm => vm.CommandProfile, v => v.TapGestureImageProfile.Command));
            d(this.OneWayBind(ViewModel, vm => vm.CommandContacts, v => v.TapGestureImageContacts.Command));
            d(this.OneWayBind(ViewModel, vm => vm.CommandItems, v => v.TapGestureImageItems.Command));
            d(this.OneWayBind(ViewModel, vm => vm.CommandMessaging, v => v.TapGestureImageMessaging.Command));
            d(this.OneWayBind(ViewModel, vm => vm.CommandSettings, v => v.TapGestureImageSettings.Command));
            d(this.OneWayBind(ViewModel, vm => vm.CommandBack, v => v.TapGestureBack.Command));

        }

        public async Task AnimationAppearing()
        {
            await ImageProfile.FadeTo(1);

            await Task.WhenAll(
                Task.WhenAll(
                    ImageSettings.FadeTo(1),
                    ImageSettings.RotateTo(0)),
                Task.WhenAll(
                    ImageItems.FadeTo(1),
                    ImageItems.RotateTo(0)),
                Task.WhenAll(
                    ImageMessaging.FadeTo(1),
                    ImageMessaging.RotateTo(0)),
                Task.WhenAll(
                    ImageContacts.FadeTo(1),
                    ImageContacts.RotateTo(0)));
        }

        public void BackToAnimation()
        {
            BackToAnimationImage(ImageSettings, 45);
            BackToAnimationImage(ImageItems, -45);
            BackToAnimationImage(ImageMessaging, 45);
            BackToAnimationImage(ImageContacts, -45);
        }

        private void BackToAnimationImage(Image image, int rotation)
        {
            image.Opacity = 0;
            image.Rotation = rotation;
        }
	}
}