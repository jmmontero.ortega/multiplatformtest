﻿using SideKickTest.Base;
using System;
using ReactiveUI;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using System.Reactive;
using System.Threading.Tasks;
using SideKickTest.Features.Contacts;
using SideKickTest.Features.Messaging;
using SideKickTest.Features.Settings;
using SideKickTest.Features.Items;
using SideKickTest.Features.Profile;
using Xamarin.Forms;

namespace SideKickTest.Features.Main
{
    public class MainViewModel : BaseViewModel
    {
        private ReactiveCommand<Unit,Unit> commandProfile;
        private ReactiveCommand<Unit, Unit> commandItems;
        private ReactiveCommand<Unit, Unit> commandSettings;
        private ReactiveCommand<Unit, Unit> commandMessaging;
        private ReactiveCommand<Unit, Unit> commandContacts;
        private ReactiveCommand<Unit, Unit> commandBack;

        public ICommand CommandProfile => commandProfile;
        public ICommand CommandItems => commandItems;
        public ICommand CommandSettings => commandSettings;
        public ICommand CommandMessaging => commandMessaging;
        public ICommand CommandContacts => commandContacts;
        public ICommand CommandBack => commandBack;
        
        protected override void InitializeCommands()
        {
            base.InitializeCommands();
            commandProfile = ReactiveCommand.CreateFromTask(PerformProfile);
            commandItems = ReactiveCommand.CreateFromTask(PerformItems);
            commandSettings = ReactiveCommand.CreateFromTask(PerformSettings);
            commandMessaging = ReactiveCommand.CreateFromTask(PerformMessaging);
            commandContacts = ReactiveCommand.CreateFromTask(PerformContacts);
            commandBack = ReactiveCommand.CreateFromTask(PerformBack);
        }
        
        private async Task PerformContacts() => await Navigation.PushAsync(new ContactsView());


        private async Task PerformMessaging() => await Navigation.PushAsync(new MessagingView());


        private async Task PerformSettings() => await Navigation.PushAsync(new SettingsView());

        private async Task PerformItems() => await Navigation.PushAsync(new ElementsView());

        private async Task PerformProfile() => await Navigation.PushAsync(new ProfileView());

        private async Task PerformBack() => MessagingCenter.Send<MainViewModel>(this, "Back");
        
    }
}
