﻿using Xamarin.Forms.Xaml;

namespace SideKickTest.Features.Messaging
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MessagingView
	{
		public MessagingView ()
		{
			InitializeComponent ();
		}
	}
}