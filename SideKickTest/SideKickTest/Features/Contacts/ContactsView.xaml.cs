﻿using Xamarin.Forms.Xaml;

namespace SideKickTest.Features.Contacts
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactsView
	{
		public ContactsView ()
		{
			InitializeComponent ();
		}
	}
}