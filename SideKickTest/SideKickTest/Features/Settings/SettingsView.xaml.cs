﻿using Xamarin.Forms.Xaml;

namespace SideKickTest.Features.Settings
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsView
	{
		public SettingsView ()
		{
			InitializeComponent ();
		}
	}
}